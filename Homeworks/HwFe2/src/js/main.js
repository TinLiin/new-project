let burger = document.querySelector(".burger-btn");
let navMenu = document.querySelector(".page-nav__list");

burger.addEventListener("click", () => {
    burger.classList.toggle("burger-btn--active");
    navMenu.classList.toggle("page-nav__list--open")
});