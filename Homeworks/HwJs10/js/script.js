"use strict";
let contentList = document.querySelectorAll(".tabs-content > li");
document.querySelector(".tabs").addEventListener("click", (el) => {
  if (el.target.closest("li")) {
    for (const i of contentList) {
      i.style.cssText = "display: none;";
      if (i.getAttribute("data-id") === el.target.getAttribute("data-id")) {
        i.style.cssText = "display: flex;";
      }
    }
  }
});
