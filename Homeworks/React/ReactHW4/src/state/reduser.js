import {
  SAVE_GOODS_LIST,
  ADD_TO_FAVORITES,
  DELL_FROM_FAVORITES,
  TOGLE_MODAL1,
  TOGLE_MODAL2,
  ADD_TO_CART,
  DELL_FROM_CART,
} from "./actions";

const initialState = {
  loading: false,
  goods: {},
  favorites: [],
  cart: [],
  starActiv: false,
  modal1: false,
  modal2: false,
};

const reduser = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_GOODS_LIST:
      return {
        ...state,
        goods: action.payload,
        loading: true,
        favorites: JSON.parse(localStorage.getItem("favorites")),
        cart: JSON.parse(localStorage.getItem("cart")),
      };
    case TOGLE_MODAL1:
      return {
        ...state,
        modal1: action.payload,
      };
    case TOGLE_MODAL2:
      return {
        ...state,
        modal2: action.payload,
      };
    case ADD_TO_FAVORITES:
      return {
        ...state,
        favorites: [...state.favorites, action.payload],
      };
    case DELL_FROM_FAVORITES:
      return {
        ...state,
        favorites: [
          ...state.favorites.slice(0, state.favorites.indexOf(action.payload)),
          ...state.favorites.slice(state.favorites.indexOf(action.payload) + 1)
        ],
      };
    case ADD_TO_CART:
      return {
        ...state,
        cart: [...state.cart, action.payload],
      };
      case DELL_FROM_CART:
        return {
          ...state,
          cart: [
            ...state.cart.slice(0, state.cart.indexOf(action.payload)),
            ...state.cart.slice(state.cart.indexOf(action.payload) + 1),
          ],
        };
    default:
      return state;
  }
};
export default reduser;
