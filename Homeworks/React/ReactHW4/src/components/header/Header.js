import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector } from "react-redux";


function Header() {
const favoritesCount = useSelector((state) => state.favorites).length;
const cartCount = useSelector((state) => state.cart).length;
    return (
<nav className="navbar navbar-expand-lg bg-light" >
  <div className="container-fluid">
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <Link className="nav-link active" aria-current="page" to="/">Home</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link active" aria-current="page" to="/favorites">Favorites</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link active" aria-current="page" to="/cart">Cart</Link>
        </li>
      </ul>

             <svg
              width="16px"
              height="16px"
              viewBox="0 0 16 16"
              className="bi bi-cart2"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"
              />
            </svg>
            <p style={{color: "red", marginRight: "15px"}}>{cartCount}</p>
             <svg
              width="20"
              height="20"
              viewBox="0 0 48 48"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect width="35" height="35" fill="white" fillOpacity="0.01" />
              <circle
                cx="24"
                cy="24"
                r="20"
                fill="#2F88FF"
                stroke="black"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M15 24H33"
                stroke="white"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M19.5 16.2058L28.5 31.7942"
                stroke="white"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M28.5 16.2058L19.5 31.7942"
                stroke="white"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <p style={{color: "red", marginRight: "15px"}}>{favoritesCount}</p>
    </div>
  </div>
</nav>
    );

}

Header.propTypes ={
  cartCount: PropTypes.number, 
  starCount: PropTypes.number
};

export default Header;
