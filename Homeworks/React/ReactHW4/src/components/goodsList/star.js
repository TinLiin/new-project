import { addToFavorites, dellFromFavorites } from "../../state/actions";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";

function Star({ article }) {
  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.favorites);
  const handleStar =  () => {
    const localStar= JSON.parse(localStorage.getItem("favorites"));
      if (favorites.includes(article)) {
        dispatch(dellFromFavorites(article));
        localStorage.setItem("favorites", JSON.stringify([
          ...localStar.slice(0, localStar.indexOf(article)),
          ...localStar.slice(localStar.indexOf(article) + 1)
        ]));
      } 
      else {
        dispatch(addToFavorites(article));
        localStorage.setItem("favorites", JSON.stringify([...localStar, article]));
      }

  };
  return (
    <>
      {!favorites.includes(article) && (
        <svg
          className="star"
          onClick={() => handleStar({ article })}
          width="48"
          height="48"
          viewBox="0 0 48 48"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect width="48" height="48" fill="white" fillOpacity="0.01" />
          <circle
            cx="24"
            cy="24"
            r="20"
            fill="#2F88FF"
            stroke="black"
            strokeWidth="4"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M15 24H33"
            stroke="white"
            strokeWidth="4"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M19.5 16.2058L28.5 31.7942"
            stroke="white"
            strokeWidth="4"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M28.5 16.2058L19.5 31.7942"
            stroke="white"
            strokeWidth="4"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      )}
      {favorites.includes(article) && (
          <svg
            className="star"
            onClick={() => handleStar({ article })}
            width="48"
            height="48"
            viewBox="0 0 48 48"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect width="48" height="48" fill="white" fillOpacity="0.01" />
            <circle
              cx="24"
              cy="24"
              r="20"
              fill="red"
              stroke="black"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M15 24H33"
              stroke="white"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M19.5 16.2058L28.5 31.7942"
              stroke="white"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M28.5 16.2058L19.5 31.7942"
              stroke="white"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        )}
    </>
  );
}
Star.propTypes = {
  article: PropTypes.number,
  handleStar: PropTypes.func,
  starActiv: PropTypes.bool,
};
export default Star;
