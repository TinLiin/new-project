import { Component } from "react";
import Button from "./components/button/Button.js";
import Modal from "./components/modal/Modal.js";
import GoodsList from "./components/goodsList/goods.js";
import Header from "./components/header/Header.js";

class App extends Component {
  state = {
    modal1: false,
    starActiv: true,
    cart: 0,
    artCount: 0,
    starCount: 0,
    goods: {},
    loading: false
  };
  componentDidMount() {
    fetch(`${process.env.PUBLIC_URL}/colection.json`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((json) => this.setState({goods: json, loading: true}));
    if (!localStorage.getItem("cart")){
      this.setState({cartCount: 0});
      localStorage.setItem("cart", JSON.stringify([]))
    }
    else {
      this.setState({cartCount: JSON.parse(localStorage.getItem("cart")).length})
    };
    if (!localStorage.getItem("favorites")){
      this.setState({starCount: 0});
      localStorage.setItem("favorites", JSON.stringify([]))
    }
    else {
      this.setState({starCount: JSON.parse(localStorage.getItem("favorites")).length})
    };
  };


  handlerModal = (e) => {
    if (
      !e.target.closest(".modalContainer") ||
      e.target.closest(".closeModal")
    ) {
      this.setState({ modal1: false, modal2: false });
    }
  };
  handleButton1 = (article) => {
      this.setState({ modal1: !this.state.modal1, cart: article.target.attributes.article.value });
  };
  handleButton2 = () => {
    this.setState({ modal2: !this.state.modal2 });
  };

  handleStar = (staredArticcle) => {
    staredArticcle = +staredArticcle.article;
    let favorites = localStorage.getItem("favorites");
    if (favorites) {
      favorites = [...JSON.parse(favorites)];
      if (favorites.includes(staredArticcle)) {
        favorites = [
          ...favorites.slice(0, favorites.indexOf(staredArticcle)),
          ...favorites.slice(favorites.indexOf(staredArticcle) + 1)
        ];
        localStorage.setItem("favorites", JSON.stringify(favorites));
        this.setState({ starActiv: true, starCount: favorites.length });
      } else {
        this.setState({ starActiv: true });
        favorites = [...favorites, staredArticcle];
        localStorage.setItem("favorites", JSON.stringify(favorites));
        this.setState({starCount: favorites.length});
      }
    } else {
      favorites = [staredArticcle];
      this.setState({ starActiv: true });
      localStorage.setItem("favorites", JSON.stringify(favorites));
      this.setState({starCount: favorites.length});
    }
  };
 

cart = () => {
      let article = +this.state.cart;
      let cart = localStorage.getItem("cart");
      if (cart) {
        cart = [...JSON.parse(cart)];
        if (cart.includes(article)) {
          this.setState({modal1: false});
        } else {
          cart = [...cart, article];
          localStorage.setItem("cart", JSON.stringify(cart));
          this.setState({modal1: false, cartCount: JSON.parse(localStorage.getItem("cart")).length});
        }
      } else {
        article = [article];
        localStorage.setItem("cart", JSON.stringify(article));
        this.setState({modal1: false, cartCount: 1});
        console.log(this.state.cartCount);
      }
  };

  render() {
    return (
      <>
        <Header cartCount={this.state.cartCount} starCount={this.state.starCount}  />
        {this.state.loading&&<GoodsList goods={this.state.goods.vegetables} starActiv={this.state.starActiv} handleStar={this.handleStar}  onClick={this.handleButton1} />}
        {this.state.modal1 && (
          <Modal
            header={"Do you wrealy want to buy this?"}
            closeButton={true}
            text={
              "this action will add the item to the cart"
            }
            actions={
              <>
                <Button backgroundColor="#b3382b" onClick={this.cart} text="OK"></Button>
                <Button backgroundColor="#b3382b" text="Cancel"></Button>
              </>
            }
            modal={this.handlerModal}
          />
        )}
        
      </>
    );
  }
}

export default App;
