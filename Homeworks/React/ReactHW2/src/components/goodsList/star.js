import { Component } from "react";
import PropTypes from "prop-types";

class Star extends Component {
  state = {
    activ: true,
  };

  render() {
    const { article, handleStar, starActiv } = this.props;
    // if (localStorage.getItem("favorites") === null) {
    //   localStorage.setItem("favorites", JSON.stringify([]));
    // }
    return (
      <>
        {!localStorage.getItem("favorites").includes(article) && (
          <svg
            className="star"
            onClick={() => handleStar({ article })}
            width="48"
            height="48"
            viewBox="0 0 48 48"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect width="48" height="48" fill="white" fillOpacity="0.01" />
            <circle
              cx="24"
              cy="24"
              r="20"
              fill="#2F88FF"
              stroke="black"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M15 24H33"
              stroke="white"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M19.5 16.2058L28.5 31.7942"
              stroke="white"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M28.5 16.2058L19.5 31.7942"
              stroke="white"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        )}
        {starActiv === true &&
          localStorage.getItem("favorites").includes(article) && (
            <svg
              className="star"
              onClick={() => handleStar({ article })}
              width="48"
              height="48"
              viewBox="0 0 48 48"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect width="48" height="48" fill="white" fillOpacity="0.01" />
              <circle
                cx="24"
                cy="24"
                r="20"
                fill="red"
                stroke="black"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M15 24H33"
                stroke="white"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M19.5 16.2058L28.5 31.7942"
                stroke="white"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M28.5 16.2058L19.5 31.7942"
                stroke="white"
                strokeWidth="4"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          )}
      </>
    );
  }
}
Star.propTypes = {
  article: PropTypes.number,
  handleStar: PropTypes.func,
  starActiv: PropTypes.bool,
};
export default Star;
