import { Component } from "react";
import  Card  from "./card";
import PropTypes from 'prop-types'

class GoodsList extends Component {

  render() {
    const { goods, onClick, handleStar, starActiv } = this.props;
     if (localStorage.getItem("cart") === null) {
      localStorage.setItem("cart", JSON.stringify([]));
    };
    return (
      <ul className="cards-container">
        {goods.map((cardObj)=>{
          return  <Card key={cardObj.article} cardObj={cardObj} handleStar={handleStar} starActiv={starActiv} onClick={onClick} />
        })}
      </ul>
    );
  }
}

GoodsList.propTypes = {
  goods: PropTypes.array, 
  onClick: PropTypes.func, 
  handleStar: PropTypes.func, 
  starActiv: PropTypes.bool
};

export default GoodsList;
