import { Component } from "react";
import Button from "../button/Button.js";
import Star from "./star";
import PropTypes from 'prop-types'

class Card extends Component {

 favorites(){
  const {onClick} = this.props;
  onClick();
 };
 
  render() {
    
    const { price, name, image, article } = this.props.cardObj;
    const {onClick, handleStar, starActiv} = this.props;
  
    return (
        <li className="card" style={{width: '18rem'}} id={article}>
          <img
            src={process.env.PUBLIC_URL + `${image}`}
            className="card-img-top"
            alt={name}
          />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h4 className="card-text">{price+"$"}</h4>
            <p className="card-text">{`art.${article}`}</p>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <Button onClick={onClick} article={article} backgroundColor="#1f47b5" text="Add to cart" />
            <Star article={+article} card={this.cardObj} starActiv={starActiv} handleStar={handleStar} />
          </div>
        </li>
    );
  }
}

Card.propTypes = {
  price: PropTypes.number, 
  name: PropTypes.string, 
  image: PropTypes.string, 
  article: PropTypes.string,
  onClick: PropTypes.func, 
  handleStar: PropTypes.func, 
  starActiv: PropTypes.bool
};

export default Card;
