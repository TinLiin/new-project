
import { applyMiddleware, combineRedusers, createStore } from "redux";
import thunk from "redux-thunk";
import reduser from "./reduser"


const store = createStore(reduser, applyMiddleware(thunk));

export default store;
