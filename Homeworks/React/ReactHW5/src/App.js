import { useState, useEffect } from "react";
import Button from "./components/button/Button.js";
import Modal from "./components/modal/Modal.js";
import GoodsList from "./components/goodsList/goods.js";
import Header from "./components/header/Header.js";
import { Route, Routes } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  sveGoodsList,
  togleModal1,
  addToCart,
  dellFromCart,
  togleModal2,
} from "./state/actions";

function App() {

  const dispatch = useDispatch();
  const goods = useSelector((state) => state.goods);
  const loading = useSelector((state) => state.loading);
  const modal1 = useSelector((state) => state.modal1);
  const modal2 = useSelector((state) => state.modal2);
  const cartList = useSelector((state) => state.cart);
  const localFavorites = JSON.parse(localStorage.getItem("favorites"));
  const localCart = JSON.parse(localStorage.getItem("cart"));

  if (localFavorites == null) {
    localStorage.setItem("favorites", JSON.stringify([]));
  }
  if (localCart == null) {
    localStorage.setItem("cart", JSON.stringify([]));
  }

  useEffect(() => {
    fetch(`${process.env.PUBLIC_URL}/colection.json`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((data) => {
        dispatch(sveGoodsList(data));
      });
  }, []);

  const handleCart = () => {
    const article = +modal1;
    if (cartList.includes(article)) {
      dispatch(togleModal1(false));
    } else {
      dispatch(addToCart(article));
      dispatch(togleModal1(false));
      const storageCart = JSON.parse(localStorage.getItem("cart"));
      localStorage.setItem("cart", JSON.stringify([...storageCart, article]));
    }
  };
  const handleDellFromCart = () => {
    let cartItems = JSON.parse(localStorage.getItem("cart"));
    cartItems = [
      ...cartItems.slice(0, cartItems.indexOf(modal2)),
      ...cartItems.slice(cartItems.indexOf(modal2) + 1),
    ];
    localStorage.setItem("cart", JSON.stringify(cartItems));
    dispatch(dellFromCart(modal2));
    dispatch(togleModal2(false));
  };

  if (loading === false) {
    return <p>LOADING...</p>;
  }

  return (
    <>
      <Header />
      <Routes>
        <Route
          path="/"
          element={
            <GoodsList goods={goods} filter={"all"} dellFromCartBtn={false} />
          }
        />
        <Route
          path="/favorites"
          element={
            <GoodsList
              goods={goods}

              filter={"favorites"}
              dellFromCartBtn={false}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <GoodsList goods={goods} filter={"cart"} dellFromCartBtn={true} />
          }
        />
      </Routes>
      {modal1 && (
        <Modal
          header={"Do you wrealy want to buy this?"}
          closeButton={true}
          text={"this action will add the item to the cart"}
          actions={
            <>
              <Button
                backgroundColor="#b3382b"
                onClick={handleCart}
                text="OK"
              ></Button>
              <Button backgroundColor="#b3382b" text="Cancel"></Button>
            </>
          }
        />
      )}
      {modal2 && (
        <Modal
          header={"Do you wrealy want to remuve this item?"}
          closeButton={true}
          text={"this action will delete item from cart"}
          actions={
            <>
              <Button
                backgroundColor="#b3382b"
                onClick={() => handleDellFromCart(modal2)}
                text="OK"
              ></Button>
              <Button backgroundColor="#b3382b" text="Cancel"></Button>
            </>
          }

        />
      )}

    </>
  );
}

export default App;
