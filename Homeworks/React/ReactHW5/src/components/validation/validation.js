import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Formik } from "formik";

import * as Yup from "yup";
import {confirmOrder} from "../../state/actions"

const schema = Yup.object().shape({
  name: Yup.string().required().min(2).max(15),
  lastName: Yup.string().required().min(2).max(15),
  age: Yup.number().required().min(16).max(120),
  adress: Yup.string().required().min(12).max(60),
  phone: Yup.number().integer().min(7).required(),
});

function Validation() {
  const cart = useSelector((state)=> state.cart);
  const goods = useSelector((state)=> state.goods);
  const dispatch = useDispatch();
  return (
    <div>
      <h1>Order Confirmation</h1>
      <Formik
        initialValues={{
          name: "Yasya",
          lastName: "Petin",
          age: 0,
          adress: "",
          phone: 0,
        }}
        onSubmit={(values) => {
          console.log(`Name: ${values.name} Last name: ${values.lastName} age: ${values.age} adress: ${values.adress} phone: ${values.phone}`);
          goods.map((cardObj)=>{
            if (cart.includes(+cardObj.article)) {
              console.log(`${cardObj.name} Article: ${cardObj.article} Color: ${cardObj.color} Price: ${cardObj.price}`);
            };
            dispatch(confirmOrder());
          })
        }}
        validationSchema={schema}
      >
        {
          ({ values, errors, handleChange, handleSubmit, handleBlur }) => (
            <form
              className="row g-3 needs-validation"
              onSubmit={handleSubmit}
              noValidate
            >
              <div className="col-md-4">
                <label htmlFor="validationCustom01" className="form-label">
                  Name
                </label>
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  id="validationCustom01"
                  value={values.name}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  required
                />
                <div>{errors.name ? errors.name : null}</div>
              </div>
              <div className="col-md-4">
                <label htmlFor="validationCustom02" className="form-label">
                  Last name
                </label>
                <input
                  type="text"
                  name="lastName"
                  className="form-control"
                  id="validationCustom02"
                  value={values.lastName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  required
                />
                <div>{errors.lastName ? errors.lastName : null}</div>
              </div>
              <div className="col-md-4">
                <label
                  htmlFor="validationCustomUsername"
                  className="form-label"
                >
                  Age
                </label>
                <div className="input-group has-validation">
                  <input
                    type="text"
                    name="age"
                    className="form-control"
                    id="validationCustomUsername"
                    aria-describedby="inputGroupPrepend"
                    value={values.age}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    required
                  />
                  <div>{errors.age ? errors.age : null}</div>
                </div>
              </div>
              <div className="col-md-6">
                <label htmlFor="validationCustom03" className="form-label">
                  Adress
                </label>
                <input
                  type="text"
                  name="adress"
                  className="form-control"
                  id="validationCustom03"
                  value={values.adress}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  required
                />
                <div>{errors.adress ? errors.adress : null}</div>
              </div>
              <div className="col-md-3">
                <label htmlFor="validationCustom05" className="form-label">
                  Phone number
                </label>
                <input
                  type="phone"
                  name="phone"
                  className="form-control"
                  id="validationCustom05"
                  value={values.phone}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  required
                />
                <div>{errors.phone ? errors.phone : null}</div>
              </div>

              <div className="col-12">
                <button className="btn btn-primary" type="submit">
                  Submit form
                </button>
              </div>
            </form>
          )
        }
      </Formik>
    </div>
  );
}

export default Validation;
