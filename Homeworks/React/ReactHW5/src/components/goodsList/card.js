import Button from "../button/Button.js";
import Star from "./star";
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from "react-redux";
import { togleModal1, togleModal2, dellFromCart } from "../../state/actions"

function Card({ cardObj, dellFromCartBtn }) {
const dispatch = useDispatch();
const price = cardObj.price;
const name = cardObj.name;
const image = cardObj.image;
const article = +cardObj.article;
const dellCart = useSelector(state=>state.modal1);
if (localStorage.getItem("cart") === null) {
  localStorage.setItem("cart", JSON.stringify([]));
};

const handleAddToCart = () => {
  dispatch(togleModal1(article));
}; 

const handledellFromCart = () => {
  dispatch(togleModal2(article));
}; 

if (dellCart !== article) {
  return (
        <li className="card" key={article} style={{width: '18rem'}}>
          {dellFromCartBtn ? (
              <button className="closeModal" onClick={handledellFromCart}>
                ✕
              </button>
            ) : null}
          <img
            src={process.env.PUBLIC_URL + `${image}`}
            className="card-img-top"
            alt={name}
          />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h4 className="card-text">{price+"$"}</h4>
            <p className="card-text">{`art.${article}`}</p>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <Button onClick={()=>handleAddToCart()} article={article} backgroundColor="#1f47b5" text="Add to cart" />
            <Star article={article} />
          </div>
        </li>
    );
}
     
}

Card.propTypes = {
  price: PropTypes.number, 
  name: PropTypes.string, 
  image: PropTypes.string, 
  article: PropTypes.number,
  onClick: PropTypes.func, 
  handleStar: PropTypes.func, 
  starActiv: PropTypes.bool
};

export default Card;
