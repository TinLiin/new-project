import Card from "./card";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import Validation from "../validation/validation";

function GoodsList({ onClick, filter, dellFromCartBtn }) {
  const goods = useSelector((state) => state.goods);
  const favorites = useSelector((state) => state.favorites);
  const cart = useSelector((state) => state.cart);
  if (filter == "favorites") {
    return (
      <ul className="cards-container">
        {goods.map((cardObj) => {
          return (
            favorites.includes(+cardObj.article) && (
              <Card key={cardObj.article} cardObj={cardObj} onClick={onClick} />
            )
          );
        })}
      </ul>
    );
  }
  if (filter == "cart") {
    return (
      <>
        <ul className="cards-container">
          {goods.map((cardObj) => {
            return (
              cart.includes(+cardObj.article) && (
                  <Card
                  key={cardObj.article}
                    cardObj={cardObj}
                    onClick={onClick}
                    dellFromCartBtn={dellFromCartBtn}/>
              )
            );
          })}
        </ul>
        <Validation />
      </>
    );
  }
  if (filter == "all") {
    return (
      <ul className="cards-container">
        {goods.map((cardObj) => {
          return (
            <Card key={cardObj.article} cardObj={cardObj} onClick={onClick} />
          );
        })}
      </ul>
    );
  }
}

GoodsList.propTypes = {
  goods: PropTypes.array,
  onClick: PropTypes.func,
  handleStar: PropTypes.func,
  starActiv: PropTypes.bool,
};

export default GoodsList;
