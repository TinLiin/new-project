
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import {togleModal1, togleModal2 } from "../../state/actions"

function Modal({ header, closeButton, text, actions }) {
const dispatch = useDispatch();
  function handlerModal(e) {
    if (
      !e.target.closest(".modalContainer") ||
      e.target.closest(".closeModal")
    ) {
      dispatch(togleModal1(false));
      dispatch(togleModal2(false));
    }
  }

  return (
    <div className="modalBackground" onClick={handlerModal}>
      <div className="modalContainer">
        <div className="modalHeader">
          <h3 className="modalHeaderText">{header}</h3>
          <>
            {closeButton ? (
              <button onClick={handlerModal} className="closeModal">
                ✕
              </button>
            ) : ("")}
          </>
        </div>
        <p className="modalText">{text}</p>
        <div className="modalButtonsContainer">{actions}</div>
      </div>
    </div>
  );
}

Modal.defaultProps = {
  text: "some text might be here",
  header: "Here might be header",
};

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.element,
  modal: PropTypes.func,
};

export default Modal;
