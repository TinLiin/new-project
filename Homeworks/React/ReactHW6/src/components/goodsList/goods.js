import Card from "./card";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import Validation from "../validation/validation";
import { useState } from "react";
import MainContext from "../../context/context";

function GoodsList({ onClick, filter, dellFromCartBtn }) {
  const goods = useSelector((state) => state.goods);
  const favorites = useSelector((state) => state.favorites);
  const cart = useSelector((state) => state.cart);
  const [column, setColumn] = useState("18rem");

  const swicherHandler = () => {
    if (column == "18rem") {
      setColumn("95%");
    } else {
      setColumn("18rem");
    }
  };

  if (filter == "favorites") {
    return (
      <MainContext.Provider value={column}>
        <ul className="cards-container">
          {goods.map((cardObj) => {
            return (
              favorites.includes(+cardObj.article) && (
                <Card
                  key={cardObj.article}
                  cardObj={cardObj}
                  onClick={onClick}
                />
              )
            );
          })}
        </ul>
      </MainContext.Provider>
    );
  }
  if (filter == "cart") {
    return (
      <MainContext.Provider value={column}>
        <>
          <ul className="cards-container">
            {goods.map((cardObj) => {
              return (
                cart.includes(+cardObj.article) && (
                  <Card
                    key={cardObj.article}
                    cardObj={cardObj}
                    onClick={onClick}
                    dellFromCartBtn={dellFromCartBtn}
                  />
                )
              );
            })}
          </ul>
          <Validation />
        </>
      </MainContext.Provider>
    );
  }
  if (filter == "all") {
    return (
      <MainContext.Provider value={column}>
        <div className="form-check form-switch form-check-reverse">
          <input
            onClick={swicherHandler}
            className="form-check-input"
            type="checkbox"
            id="flexSwitchCheckReverse"
          />
          <label className="form-check-label" htmlFor="flexSwitchCheckReverse">
            Display products in a column
          </label>
        </div>
        <ul className="cards-container">
          {goods.map((cardObj) => {
            return (
              <Card key={cardObj.article} cardObj={cardObj} onClick={onClick} />
            );
          })}
        </ul>
      </MainContext.Provider>
    );
  }
}

GoodsList.propTypes = {
  goods: PropTypes.array,
  onClick: PropTypes.func,
  handleStar: PropTypes.func,
  starActiv: PropTypes.bool,
};

export default GoodsList;
