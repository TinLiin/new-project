export const SAVE_GOODS_LIST = "SAVE_GOODS_LIST";
export const ADD_TO_FAVORITES = "ADD_TO_FAVORITES";
export const DELL_FROM_FAVORITES = "DELL_FROM_FAVORITES";
export const TOGLE_MODAL1 = "TOGLE_MODAL1";
export const TOGLE_MODAL2 = "TOGLE_MODAL2";
export const ADD_TO_CART = "ADD_TO_CART";
export const DELL_FROM_CART = "DELL_FROM_CART";
export const CONFIRM_ORDER = "CONFIRM_ORDER";

export function sveGoodsList(data) {
    return {
        type: SAVE_GOODS_LIST,
        payload: data
    }
};

export function togleModal1(article) {
    return {
        type: TOGLE_MODAL1,
        payload: article
    }
};

export function togleModal2(article) {
    return {
        type: TOGLE_MODAL2,
        payload: article
    }
};

export function addToFavorites(article) {
    return {
        type: ADD_TO_FAVORITES,
        payload: article
    }
};

export function dellFromFavorites(article) {
    return {
        type: DELL_FROM_FAVORITES,
        payload: article
    }
};

export function addToCart(article) {
    return {
        type: ADD_TO_CART,
        payload: article
    }
};

export function dellFromCart(article) {
    return {
        type: DELL_FROM_CART,
        payload: article
    }
};

export function confirmOrder() {
localStorage.setItem("cart", JSON.stringify([]))
    return {
        type: CONFIRM_ORDER,
    }
};