import Modal from "../components/modal/Modal";
import { render } from "@testing-library/react";
import * as reduxHooks from "react-redux";

jest.mock("react-redux");

describe("Test modal window", () => {
  it("have to create modal", () => {
    jest.spyOn(reduxHooks, "useDispatch").mockResolvedValue(jest.fn());
    const component = render(<Modal />);
    expect(component).toMatchSnapshot();
  });
  it("have to render modal", () => {
    jest.spyOn(reduxHooks, "useDispatch").mockResolvedValue(jest.fn());
    render(<Modal />);
  });
});
