import Validation from "../components/validation/validation";
import { render } from "@testing-library/react";
import * as reduxHooks from "react-redux";


jest.mock("react-redux");

describe("validation test", ()=>{
    it("should render form", ()=>{
        jest.spyOn(reduxHooks, "useDispatch").mockResolvedValue(jest.fn());
    jest.spyOn(reduxHooks, "useSelector").mockReturnValue([]);
    render(<Validation/>);
    });
    it("should render form", ()=>{
        jest.spyOn(reduxHooks, "useDispatch").mockResolvedValue(jest.fn());
    jest.spyOn(reduxHooks, "useSelector").mockReturnValue([]);
    const component = render(<Validation/>);
    expect(component).toMatchSnapshot();
    });
})