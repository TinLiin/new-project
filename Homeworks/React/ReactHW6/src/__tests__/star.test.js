import Star from "../components/goodsList/star";
import { render } from "@testing-library/react";
import * as reduxHooks from "react-redux";


jest.mock("react-redux");

describe("star test", ()=>{
it("have to render star", ()=>{
    jest.spyOn(reduxHooks, "useDispatch").mockResolvedValue(jest.fn());
    jest.spyOn(reduxHooks, "useSelector").mockReturnValue([]);
    render(<Star/>);
});
it("have to render star", ()=>{
    jest.spyOn(reduxHooks, "useDispatch").mockResolvedValue(jest.fn());
    jest.spyOn(reduxHooks, "useSelector").mockReturnValue([]);
    const component = render(<Star/>);
    expect(component).toMatchSnapshot();
});
});