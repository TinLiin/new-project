import reduser from "../state/reduser";

const initState = {
  loading: false,
  goods: {},
  favorites: [],
  cart: [],
  starActiv: false,
  modal1: false,
  modal2: false,
};

describe("Reducer test", () => {
  it("Reducer initial state test", () => {
    const result = reduser(undefined, { fetch: "" });
    expect(result).toEqual(initState);
  });
  it("it should togle modsl1", () => {
    const action = { type: "TOGLE_MODAL1", payload: 123456 };
    const result = reduser(initState, action);
    expect(result.modal1).toBe(123456);
  });
  it("it should add to favorites", () => {
    const action = { type: "ADD_TO_FAVORITES", payload: 123456 };
    const result = reduser(initState, action);
    expect(result.favorites).toEqual([123456]);
  });
});
