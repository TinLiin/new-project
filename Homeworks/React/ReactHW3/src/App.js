import { useState, useEffect } from "react";
import Button from "./components/button/Button.js";
import Modal from "./components/modal/Modal.js";
import GoodsList from "./components/goodsList/goods.js";
import Header from "./components/header/Header.js";
import { Route, Routes } from "react-router-dom";

function App() {
  const [modal1, setModal1] = useState(false);
  const [modal2, setModal2] = useState(false);
  const [starActiv, setStarActiv] = useState(true);
  const [cart, setCart] = useState(0);
  const [dellCart, setDellCart] = useState(false);
  const [cartCount, setCartCount] = useState(0);
  const [starCount, setStarCount] = useState(0);
  const [goods, setGoods] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetch(`${process.env.PUBLIC_URL}/colection.json`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((data) => {
        setGoods(data);
        setLoading(true);
      });
      if (!localStorage.getItem("cart")){
        setCartCount(0);
        localStorage.setItem("cart", JSON.stringify([]))
      }
      else {
        setCartCount(JSON.parse(localStorage.getItem("cart")).length);
      };
      if (!localStorage.getItem("favorites")){
        setStarCount(0);
        localStorage.setItem("favorites", JSON.stringify([]))
      }
      else {
        setStarCount(JSON.parse(localStorage.getItem("favorites")).length);
      };
  }, []);

  function handlerModal(e) {
    if (
      !e.target.closest(".modalContainer") ||
      e.target.closest(".closeModal")
    ) {
      setModal1(false);
      setModal2(false);
    }
  }

  const handleButton1 = (article) => {
    setModal1(!modal1);
    setCart(article.target.attributes.article.value);
  };
  const handleModal2 = (article)=>{
    setModal2(article);
}
  const handleStar = (staredArticcle) => {
    staredArticcle = +staredArticcle.article;
    let favorites = localStorage.getItem("favorites");
    if (favorites) {
      favorites = [...JSON.parse(favorites)];
      if (favorites.includes(staredArticcle)) {
        favorites = [
          ...favorites.slice(0, favorites.indexOf(staredArticcle)),
          ...favorites.slice(favorites.indexOf(staredArticcle) + 1)
        ];
        localStorage.setItem("favorites", JSON.stringify(favorites));
        setStarActiv(true);
        setStarCount(favorites.length);
      } else {
        setStarActiv(true);
        favorites = [...favorites, staredArticcle];
        localStorage.setItem("favorites", JSON.stringify(favorites));
        setStarCount(favorites.length);
      }
    } else {
      favorites = [staredArticcle];
      setStarActiv(true);
      localStorage.setItem("favorites", JSON.stringify(favorites));
      setStarCount(favorites.length);
    }
  };

  const handleCart = () => {
    let article = +cart;
    let cartList = localStorage.getItem("cart");
    if (cartList) {
      cartList = [...JSON.parse(cartList)];
      if (cartList.includes(article)) {
        setModal1(false);
      } else {
        cartList = [...cartList, article];
        localStorage.setItem("cart", JSON.stringify(cartList));
        setModal1(false);
        setCartCount(JSON.parse(localStorage.getItem("cart")).length);
      }
    } else {
      article = [article];
      localStorage.setItem("cart", JSON.stringify(article));
      setModal1(false);
      setCartCount(1);
    }
  };
  const handleDellFromCart = (article)=> {
    let cartItems = JSON.parse(localStorage.getItem("cart"));
    cartItems = [
      ...cartItems.slice(0, cartItems.indexOf(article)),
      ...cartItems.slice(cartItems.indexOf(article) + 1)
    ];
    localStorage.setItem("cart", JSON.stringify(cartItems));
    setCartCount(cartItems.length);
    setDellCart(article);
    setModal2(false);
  };

  if (loading === false) {
    return <p>LOADING...</p>;
  }

  return (
    <>
      <Header cartCount={cartCount} starCount={starCount} />
      <Routes>
        <Route
          path="/"
          element={
            <GoodsList
              goods={goods.vegetables}
              starActiv={starActiv}
              handleStar={handleStar}
              onClick={handleButton1}
              filter={"all"}
              dellFromCartBtn={false}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <GoodsList
              goods={goods.vegetables}
              starActiv={starActiv}
              handleStar={handleStar}
              onClick={handleButton1}
              filter={"favorites"}
              dellFromCartBtn={false}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <GoodsList
              goods={goods.vegetables}
              starActiv={starActiv}
              handleStar={handleStar}
              onClick={handleButton1}
              filter={"cart"}
              dellFromCart={handleModal2}
              dellFromCartBtn={true}
              dellCart={dellCart}
            />
          }
        />
      </Routes>
      {modal1 && (
        <Modal
          header={"Do you wrealy want to buy this?"}
          closeButton={true}
          text={"this action will add the item to the cart"}
          actions={
            <>
              <Button
                backgroundColor="#b3382b"
                onClick={handleCart}
                text="OK"
              ></Button>
              <Button backgroundColor="#b3382b" text="Cancel"></Button>
            </>
          }
          modal={handlerModal}
        />
      )}
      {modal2 && (
        <Modal
          header={"Do you wrealy want to remuve this item?"}
          closeButton={true}
          text={"this action will delete item from cart"}
          actions={
            <>
              <Button
                backgroundColor="#b3382b"
                onClick={()=>handleDellFromCart(modal2)}
                text="OK"
              ></Button>
              <Button backgroundColor="#b3382b" text="Cancel"></Button>
            </>
          }
          modal={handlerModal}
        />
      )}
    </>
  );
}

export default App;
