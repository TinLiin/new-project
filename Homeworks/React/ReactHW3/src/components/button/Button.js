import PropTypes from 'prop-types'

function Button ({ article, onClick, backgroundColor, text }) {
  
    return (
      <button
        type="button"
        className="button"
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
        article={article}
      >
        {text}
      </button>
    );
};

Button.defaultProps = {
  backgroundColor: "#5555",
  text: "Ok"
};

Button.propTypes = {
  article: PropTypes.string, 
  onClick: PropTypes.func, 
  backgroundColor: PropTypes.string, 
  text: PropTypes.string
};

export default Button;
