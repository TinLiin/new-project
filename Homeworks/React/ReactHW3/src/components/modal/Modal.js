
import PropTypes from "prop-types";

function Modal({ header, closeButton, text, actions, modal }) {

  return (
    <div className="modalBackground" onClick={modal}>
      <div className="modalContainer">
        <div className="modalHeader">
          <h3 className="modalHeaderText">{header}</h3>
          <>
            {closeButton ? (
              <button onClick={modal} className="closeModal">
                ✕
              </button>
            ) : ("")}
          </>
        </div>
        <p className="modalText">{text}</p>
        <div className="modalButtonsContainer">{actions}</div>
      </div>
    </div>
  );
}

Modal.defaultProps = {
  text: "some text might be here",
  header: "Here might be header",
};

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.element,
  modal: PropTypes.func,
};

export default Modal;
