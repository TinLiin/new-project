import Card from "./card";
import PropTypes from "prop-types";

function GoodsList({ goods, onClick, handleStar, starActiv, filter, dellFromCartBtn, dellFromCart, dellCart }) {

  if (filter == "favorites") {
    return (
      <ul className="cards-container">
        {goods.map((cardObj) => {
          return (
            localStorage.getItem("favorites").includes(cardObj.article) && (
              <Card
                key={cardObj.article}
                cardObj={cardObj}
                handleStar={handleStar}
                starActiv={starActiv}
                onClick={onClick}
              />
            )
          );
        })}
      </ul>
    );
  };
  if (filter == "cart") {
    return (
      <ul className="cards-container">
        {goods.map((cardObj) => {
          return (
            localStorage.getItem("cart").includes(cardObj.article) && (
              <Card
                key={cardObj.article}
                cardObj={cardObj}
                handleStar={handleStar}
                starActiv={starActiv}
                onClick={onClick}
                dellFromCartBtn={dellFromCartBtn}
                dellFromCart={dellFromCart}
                dellCart={dellCart}
              />
            )
          );
        })}
      </ul>
    );
  };
  if (filter == "all") {
    return (
      <ul className="cards-container">
        {goods.map((cardObj) => {
          return (
            <Card
              key={cardObj.article}
              cardObj={cardObj}
              handleStar={handleStar}
              starActiv={starActiv}
              onClick={onClick}
            />
          );
        })}
      </ul>
    );
  }
}

GoodsList.propTypes = {
  goods: PropTypes.array,
  onClick: PropTypes.func,
  handleStar: PropTypes.func,
  starActiv: PropTypes.bool,
};

export default GoodsList;
