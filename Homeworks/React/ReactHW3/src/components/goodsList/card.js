import Button from "../button/Button.js";
import Star from "./star";
import PropTypes from 'prop-types';

function Card({onClick, handleStar, starActiv, cardObj, dellFromCartBtn, dellFromCart, dellCart }) {
const price = cardObj.price;
const name = cardObj.name;
const image = cardObj.image;
const article = cardObj.article;

if (localStorage.getItem("cart") === null) {
  localStorage.setItem("cart", JSON.stringify([]));
}
if (dellCart !== article) {
  return (
        <li className="card" style={{width: '18rem'}} id={article}>
          {dellFromCartBtn ? (
              <button className="closeModal" onClick={()=>dellFromCart(+article)}>
                ✕
              </button>
            ) : ("")}
          <img
            src={process.env.PUBLIC_URL + `${image}`}
            className="card-img-top"
            alt={name}
          />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h4 className="card-text">{price+"$"}</h4>
            <p className="card-text">{`art.${article}`}</p>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <Button onClick={onClick} article={article} backgroundColor="#1f47b5" text="Add to cart" />
            <Star article={+article} card={cardObj} starActiv={starActiv} handleStar={handleStar} />
          </div>
        </li>
    );
}
     
}

Card.propTypes = {
  price: PropTypes.number, 
  name: PropTypes.string, 
  image: PropTypes.string, 
  article: PropTypes.string,
  onClick: PropTypes.func, 
  handleStar: PropTypes.func, 
  starActiv: PropTypes.bool
};

export default Card;
