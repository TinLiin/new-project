import { Component } from "react";

class Button extends Component {
  
  render() {
    const { children, onClick, backgroundColor, text } = this.props;
    return (
      <button
        type="button"
        className="button"
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

export default Button;
