import { Component, handlerModal } from "react";

class Modal extends Component {
  closeModal = (e) => {
    if (!e.target.closest(".modalContainer") || e.target.closest(".closeModal"))
    {
      this.handlerModal();
    }
    
  }
  render() {
    const { header, closeButton, text, actions, modal} = this.props;
  
    return (
      <div className="modalBackground" onClick={modal}>
        <div className="modalContainer">
          <div className="modalHeader">
            <h3 className="modalHeaderText">{header}</h3>
            <>{closeButton ? <button onClick={modal} className="closeModal">✕</button> : ""}</>
          </div>
          <p className="modalText">{text}</p>
          <div className="modalButtonsContainer">{actions}</div>
        </div>
      </div>
    );

  }
}
export default Modal;
