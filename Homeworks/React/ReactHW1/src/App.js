import { Component } from "react";
import Button from "./components/button/Button.js";
import Modal from "./components/modal/Modal.js";

class App extends Component {
  state = {
    modal1: false,
    modal2: false,
  };
  handlerModal = (e) => {
    if (
      !e.target.closest(".modalContainer") ||
      e.target.closest(".closeModal")
    ) {
      this.setState({ modal1: false, modal2: false });
    }
  };
  handleButton1 = () => {
    this.setState({ modal1: !this.state.modal1 });
  };
  handleButton2 = () => {
    this.setState({ modal2: !this.state.modal2 });
  };

  render() {
    return (
      <>
        <Button
          onClick={this.handleButton1}
          text="Open first modal"
          backgroundColor="rgb(47, 170, 170)"
        />
        <Button
          onClick={this.handleButton2}
          text="Open second modal"
          backgroundColor="rgb(81, 181, 48)"
        />
        {this.state.modal1 && (
          <Modal
            header={"Do you want to delete this file?"}
            closeButton={true}
            text={
              "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
            }
            actions={
              <>
                <Button backgroundColor="#b3382b" text="OK"></Button>
                <Button backgroundColor="#b3382b" text="Cancel"></Button>
              </>
            }
            modal={this.handlerModal}
          />
        )}
        {this.state.modal2 && (
          <Modal
            header={"Do you want to open new Window?"}
            closeButton={false}
            text={"You are opening new window"}
            actions={
              <>
                <Button backgroundColor="yellow" text="YES" />
                <Button backgroundColor="blue" text="NO" />
              </>
            }
            modal={this.handlerModal}
          />
        )}
      </>
    );
  }
}

export default App;
