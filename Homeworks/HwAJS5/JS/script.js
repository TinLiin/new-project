"use strict";
const url = "https://ajax.test-danit.com/api/json/";
const root = document.querySelector("body");

class Card {
  constructor(url, root) {
    this.url = url;
    this.root = root;
    this.posts = [];
  }
  reqest(entity, id) {
    return fetch(`${this.url}${entity}/${(id = "")}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .catch((e) => {
        console.log(e);
      });
  }
  async render() {
    await this.reqest("posts").then((data) => {
      data.forEach((el) => {
        const fragment = document.querySelector("#create-post").content;
        const newCard = fragment.querySelector(".post").cloneNode(true);
        newCard.setAttribute("data-userId", el.userId);
        newCard.querySelector("button").id = el.id;
        newCard.id = el.id;
        const postHeader = newCard.querySelector(".header");
        postHeader.innerText = el.title;
        const postText = newCard.querySelector(".post-text");
        postText.innerText = el.body;
        this.posts.push(newCard);
        root.prepend(newCard);
        newCard.addEventListener("click", (e) => {
          if (e.target.closest("button")) {
            fetch(`https://ajax.test-danit.com/api/json/posts/${e.target.id}`, {
              method: "DELETE",
              headers: {
                "Content-Type": "application/json",
              },
            })
              .then((response) => {
                if (response.ok) {
                  newCard.remove();
                }
              })
              .catch((e) => {
                console.log(e);
              });
          }
        });
      });
    });
    this.addNames();
  }
  async addNames() {
    const users = await this.reqest("users").then((data) => {
      return data;
    });
    this.posts.forEach((e) => {
      users.forEach((el) => {
        if (+e.getAttribute("data-userId") === el.id) {
          let email = e.querySelector(".mail");
          email.innerText = el.email;
          let name = e.querySelector(".post-name");
          name.innerText = el.name;
        }
      });
    });
  }
}

const card = new Card(url);
card.render();
