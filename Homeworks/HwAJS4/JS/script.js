"use strict";
const url = "https://ajax.test-danit.com/api/swapi/";
const root = document.querySelector("#root");

class StarrWarsFilms {
  constructor(url, root) {
    this.url = url;
    this.root = root;
    this.characters = [];
    this.filmsList = [];
  }
  reqest(entity, id) {
    return fetch(`${this.url}${entity}/${(id = "")}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    let filmList = document.createElement("ul");
    this.reqest("films").then((data) => {
      let films = data.map(({ episodeId, name, openingCrawl, characters }) => {
        let item = document.createElement("li");
        let desc = document.createElement("p");
        item.innerText = `Episod ${episodeId} ${name}`;
        this.characters.push(characters);
        desc.innerText = openingCrawl;
        item.append(desc);
        return item;
      });
      filmList.append(...films);
      this.filmsList.push(...films);
    });
    this.root.append(filmList);
    this.renderCharecters();
  }
  renderCharecters() {
    this.reqest("people")
      .then((data) => {
        this.filmsList.forEach((element, i) => {
          const charactersID = this.characters[i].map((el) => {
            return +el.split(
              "https://ajax.test-danit.com/api/swapi/people/"
            )[1];
          });
          let charactersList = document.createElement("ul");
          charactersList.innerText = "Characters:";
          charactersID.forEach((id)=>{
            data.forEach((e) => {if (e.id === id) {
              let listItem = document.createElement("li");
            listItem.innerText = e.name;
            charactersList.append(listItem);}
            })
          });
          element.firstChild.after(charactersList);
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }
}
const starrWarsFilms = new StarrWarsFilms(url, root).render();
