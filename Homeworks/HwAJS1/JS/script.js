
class Employee {
      constructor (name, age, salary){
      this.name = name;
      this.age = age;
      this.salary = salary;
    };
    set name(value) {
      this._name = value;
    };
    get name(){
        return this._name;
    };
    set age(value) {
      this._age = value;
    };
    get age(){
        return this._age;
    };
    set salary(value) {
      this._salary = value;
    };
    get salary(){
        return this._salary;
    }
};

class Programmer extends Employee {
  constructor (name, age, salary, lang){
    super (name, age, salary);
    this.lang = lang;
  };
  set salary(value){
    this._salary = value;
  };
  get salary(){
    return this._salary*3;
  }
};

let sasha = new Programmer("Sasha", 28, 1000, "Eng, Ukr");
console.log(sasha);
console.log(sasha.salary);
let kolya = new Programmer("Kolya", 99, 300, "Eng, Ukr, Pl");
console.log(kolya);
console.log(kolya.salary);
let petya = new Programmer("Petya", 14, 100000, "Jpn, Udmurtskiy, Buryatsky");
console.log(petya);
console.log(petya.salary);
