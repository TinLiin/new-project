
const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

let domElem = document.querySelector("#root");

function addList () {
    for (const i of books) {
    let list = document.createElement("ul");
    list.innerText = "Книга"
    try {
        if (!i.author) {throw 'ERROR!:No Author'};
        if (!i.name) {throw 'ERROR!:No name of book'};
        if (!i.price) {throw 'ERROR!:No price of book'};
    } catch (error){
      console.log(error);
      continue
    }
    let itemName = document.createElement("li");
    itemName.innerText = i.name;
    list.append(itemName)
    let itemAutor = document.createElement("li");
    itemAutor.innerText = i.author;
    list.append(itemAutor);
    let itemPrice = document.createElement("li");
    itemPrice.innerText = i.price + "$";
    list.append(itemPrice);
    domElem.append(list)
  }

}
addList();