"use strict";
let btnList = [...document.querySelectorAll("button")];
window.addEventListener("keydown", (e) => {
  for (const i of btnList) {
    if (e.key.toUpperCase() == i.innerText.toUpperCase()) {
      i.style.backgroundColor = "blue";
    } else {
      i.style.backgroundColor = "black";
    }
  }
});
