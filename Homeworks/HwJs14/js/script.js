"use strict";
let theme = document.createElement("button");
theme.textContent = "Змінити тему";
document.body.append(theme);
if (localStorage.getItem("nightMode") === "true") {
  document.querySelector(".container").style.cssText = "background: #000000";
  document.querySelector(".logo").style.cssText = "color: #ffffff";
  document.querySelectorAll(".main_text")[0].style.cssText =
    "color: lawngreen;";
  document.querySelectorAll(".main_text")[1].style.cssText =
    "color: lawngreen;";
  document.querySelector(".aside_menu").style.cssText =
    "border: 1px solid lawngreen;";
  document.querySelector(".picture_conteiner").style.cssText =
    "border: 1px solid lawngreen;";
}
console.log(localStorage.getItem("nightMode"));
theme.addEventListener("click", () => {
  if (localStorage.getItem("nightMode") === "true") {
   
document.querySelector(".container").style.cssText = "background: #ffffff";
    document.querySelector(".logo").style.cssText = "color: #000000";
    document.querySelectorAll(".main_text")[0].style.cssText =
      "color: #2c2c2c;";
    document.querySelectorAll(".main_text")[1].style.cssText =
      "color: #2c2c2c;";
    document.querySelector(".aside_menu").style.cssText =
      "border: 1px solid rgba(0, 0, 0, 1);";
    document.querySelector(".picture_conteiner").style.cssText =
      "border: 1px solid rgba(0, 0, 0, 1);";
    localStorage.setItem("nightMode", "false");
  } else {
     document.querySelector(".container").style.cssText = "background: #000000";
    document.querySelector(".logo").style.cssText = "color: #ffffff";
    document.querySelectorAll(".main_text")[0].style.cssText =
      "color: lawngreen;";
    document.querySelectorAll(".main_text")[1].style.cssText =
      "color: lawngreen;";
    document.querySelector(".aside_menu").style.cssText =
      "border: 1px solid lawngreen;";
    document.querySelector(".picture_conteiner").style.cssText =
      "border: 1px solid lawngreen;";
    localStorage.setItem("nightMode", "true");
  }
});
