"use strict";
const url = "https://api.ipify.org/?format=json";
const url2 = "http://ip-api.com/json/";
const root = document.querySelector("body");

class Hack {
  constructor(url, url2, root) {
    this.url = url;
    this.url2 = url2;
    this.root = root;
    this.ip = "";
  }
  async reqest() {
    const requestIP = await fetch(this.url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const ip = await requestIP.json();
    this.ip = ip.ip;
    
    const requestAdress = await fetch(`${this.url2}${this.ip}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
    const hackInfo = await requestAdress.json();
    const list = document.createElement("ul");
    list.style.listStyle = "none";
        for (const key in hackInfo) {
          if (key == "country" || key == "region" || key == "regionName" || key == "city" || key == "timezone") {
          const item = document.createElement("li");
          item.innerText = `${key}: ${hackInfo[key]}`;
          list.append(item);
          }
        }
        root.append(list);
  }
  lisener() {
    const button = root.querySelector("button");
    button.addEventListener("click", () => {
      this.reqest();
    });
  }
}

const hack = new Hack(url, url2, root);
hack.lisener();
