"use strict";
let allP = [...document.getElementsByTagName("p")];
console.log(allP);
allP.forEach((element) => {
  element.style.backgroundColor = "#ff0000";
});
let options = document.querySelector("#optionsList");
console.log(options);
console.log(options.parentElement);
let childs = [...options.childNodes];
childs.forEach((element) => {
  console.log(element.nodeType);
  console.log(element.nodeName);
});

let parag = document.getElementById("testParagraph");
parag.textContent = "This is a paragraph";

let mHeaderChilds = [...document.querySelector(".main-header").childNodes];
console.log(mHeaderChilds);
for (let i = 0; i < mHeaderChilds.length; i++) {
  mHeaderChilds[i].className = "nav-item";
}

let allTitles = document.querySelectorAll(".section-title");
console.log(allTitles);
allTitles.forEach(element => {
  element.classList.remove("section-title")
});
