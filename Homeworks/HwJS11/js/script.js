"use strict";
let labelList = [...document.querySelectorAll("label")];
let linkList = [...document.querySelectorAll("i")];
let inputList = [...document.querySelectorAll("input")];
let passForm = document.querySelector("form");
let warning = document.createElement("span");
warning.textContent = "Потрібно ввести однакові значення";
warning.style.color = "red";
labelList[0].addEventListener("mousedown", (e) => {
  if (e.target.closest("i")) {
    linkList[0].className = "fas fa-eye-slash icon-password";
    inputList[0].setAttribute("type", "text");
  }
});
labelList[0].addEventListener("mouseup", (e) => {
    if (e.target.closest("i")) {
      linkList[0].className = "fas fa-eye icon-password";
      inputList[0].setAttribute("type", "password");
    }
  });
  labelList[1].addEventListener("mousedown", (e) => {
    if (e.target.closest("i")) {
      linkList[1].className = "fas fa-eye-slash icon-password";
      inputList[1].setAttribute("type", "text");
    }
  });
  labelList[1].addEventListener("mouseup", (e) => {
      if (e.target.closest("i")) {
        linkList[1].className = "fas fa-eye icon-password";
        inputList[1].setAttribute("type", "password");
      }
    });
passForm.addEventListener("submit", (e)=>{
    e.preventDefault();
    warning.remove();
    if (inputList[0].value === inputList[1].value && inputList[0].value !== "") {
        alert("You are welcome");
    }
    else {
        inputList[1].after(warning);
    }
});