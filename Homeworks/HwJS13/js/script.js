"use strict";
let pictureList = [...document.querySelectorAll("img")];
let i = 0;
let paused = false;
let stop = document.createElement("button");
stop.textContent = "Припинити";
document.body.append(stop);
let play = document.createElement("button");
play.textContent = "Відновити показ";
document.body.append(play);
setInterval(player, 3000);
function player() {
  if (!paused) {
    if ((pictureList[i].className = "image-to-show" && i !== 3)) {
      pictureList[i].className = "image-to-hide";
      i = i + 1;
      pictureList[i].className = "image-to-show";
    } else if (i === 3) {
      pictureList[i].className = "image-to-hide";
      i = 0;
      pictureList[i].className = "image-to-show";
    }
  }
}
stop.addEventListener("click", () => {
  paused = true;
});
play.addEventListener("click", () => {
  paused = false;
});

