"use strict";
function showList(arr, target) {
  let list = document.createElement("ul");
  arr.forEach((element) => {
    let listItem = document.createElement("li");
    listItem.innerText = `${element}`;
    list.append(listItem);
  });
  if (target) {
    let destination = document.createElement(target);
    destination.append(list);
    document.body.append(destination);
  } else {
    document.body.append(list);
  }
}
showList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], "p");
