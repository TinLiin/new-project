"use strict";
// Task1
// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
// let [client1, client2, client3, client4] = clients2;
// let neBase = [...new Set(clients1).add(client1).add(client2).add(client3).add(client4)];
// console.log(neBase);

// Task2
// const characters = [
//   {
//     name: "Елена",
//     lastName: "Гилберт",
//     age: 17, 
//     gender: "woman",
//     status: "human"
//   },
//   {
//     name: "Кэролайн",
//     lastName: "Форбс",
//     age: 17,
//     gender: "woman",
//     status: "human"
//   },
//   {
//     name: "Аларик",
//     lastName: "Зальцман",
//     age: 31,
//     gender: "man",
//     status: "human"
//   },
//   {
//     name: "Дэймон",
//     lastName: "Сальваторе",
//     age: 156,
//     gender: "man",
//     status: "vampire"
//   },
//   {
//     name: "Ребекка",
//     lastName: "Майклсон",
//     age: 1089,
//     gender: "woman",
//     status: "vempire"
//   },
//   {
//     name: "Клаус",
//     lastName: "Майклсон",
//     age: 1093,
//     gender: "man",
//     status: "vampire"
//   }
// ];
// let charactersShortInfo = [];
// characters.forEach(e => {
//   let {name, lastName, age} = e;
//   let newChar = {
//     "name": name,
//     "lastName": lastName,
//     "age": age,
//   }
//   charactersShortInfo.push(newChar);
// });
// console.log(charactersShortInfo);

// Task3
// const user1 = {
//   name: "John",
//   years: 30,
// };
// let {name: imya, years: vik, isAdmin="false"} = user1;
// console.log(imya);
// console.log(vik);
// console.log(isAdmin);

// Task4

// const satoshi2020 = {
//   name: 'Nick',
//   surname: 'Sabo',
//   age: 51,
//   country: 'Japan',
//   birth: '1979-08-21',
//   location: {
//     lat: 38.869422, 
//     lng: 139.876632
//   }
// };

// const satoshi2019 = {
//   name: 'Dorian',
//   surname: 'Nakamoto',
//   age: 44,
//   hidden: true,
//   country: 'USA',
//   wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//   browser: 'Chrome'
// };

// const satoshi2018 = {
//   name: 'Satoshi',
//   surname: 'Nakamoto', 
//   technology: 'Bitcoin',
//   country: 'Japan',
//   browser: 'Tor',
//   birth: '1975-04-05'
// };

// Var1
// let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
// console.log(fullProfile);

// Var2
//  let fullProfile = satoshi2020;
//  function merge (donor){
// let arr = Object.entries(donor);
// arr.forEach((el)=>{
//   let [property, value] = el;
//   if (!(property in fullProfile)) {
//     fullProfile[property] = value;
//   }
//   })
// }
//  merge(satoshi2019);
//  merge(satoshi2018);
//  console.log(fullProfile);

 // Task5
//  const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
//   }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
//   }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
//   }];
  
//   const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
//   }
//   let newBooks = [...books, bookToAdd];
//   console.log(newBooks);

   // Task6
//    const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
//   }
//   const newEmployee = {...employee, age: 99, salary:100000};
//   console.log(newEmployee);

   // Task7
// const array = ['value', () => 'showValue'];

// Допишіть код тут

// alert(array[0]); // має бути виведено 'value'
// alert(array[1]());  // має бути виведено 'showValue'